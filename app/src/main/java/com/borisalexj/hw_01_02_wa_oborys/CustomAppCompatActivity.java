package com.borisalexj.hw_01_02_wa_oborys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public abstract class CustomAppCompatActivity extends AppCompatActivity implements View.OnClickListener {
    final String NAME = "";
    String TAG = "hw_01_02_wa_oborys." + this.getClass().getSimpleName();
    TextView infoTextView;
    String currentState = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);

        ((Button) findViewById(R.id.nextButton)).setOnClickListener(this);

        infoTextView = (TextView) findViewById(R.id.infoTextView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomAppCompatActivity.this.finish();
            }
        });

        if (savedInstanceState != null) {
            Log.d(TAG, "onCreate: savedInstanceState != null");
            currentState = savedInstanceState.getString("currentState");
        } else {
            Log.d(TAG, "onCreate: savedInstanceState == null");
            Intent incomingIntent = getIntent();
            currentState = incomingIntent.getStringExtra("currentState");
        }
        if (currentState == null){
            currentState = "";
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("currentState", currentState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        currentState = intent.getStringExtra("currentState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_a, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public abstract void onClick(View v);
}

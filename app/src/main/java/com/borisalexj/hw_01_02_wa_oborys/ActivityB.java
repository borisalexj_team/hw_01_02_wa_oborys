package com.borisalexj.hw_01_02_wa_oborys;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ActivityB extends CustomAppCompatActivity implements View.OnClickListener {
    final String NAME = "B";
    String TAG = "hw_01_02_wa_oborys." + this.getClass().getSimpleName();

    @Override
    protected void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.activityNameTextView)).setText(NAME);
        infoTextView.setText(currentState + " " + NAME + " " + getString(R.string.arrow));
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: ");
        if (v.getId() == R.id.nextButton){
            Intent intent = new Intent(this, ActivityC.class);
            intent.putExtra("currentState", currentState + " " + NAME + " " + getString(R.string.arrow));
            startActivity(intent);
        }

    }
}
